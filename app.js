const express = require('express');
const bodyParser = require('body-parser');

const app = express();
const PORT = process.env.PORT || 3000;

app.use(bodyParser.urlencoded({ extended: true }));

app.get('/', (req, res) => {
  res.send('Hello, this is a simple Node.js web application!');
});

app.get('/register', (req, res) => {
  res.send(`
    <h1>Registration Form</h1>
    <form action="/register" method="post">
      <label for="username">Username:</label><br>
      <input type="text" id="username" name="username"><br>
      <label for="email">Email:</label><br>
      <input type="email" id="email" name="email"><br>
      <label for="password">Password:</label><br>
      <input type="password" id="password" name="password"><br>
      <input type="submit" value="Register">
    </form>
  `);
});

app.post('/register', (req, res) => {
  const { username, email, password } = req.body;
  res.send(`Registration successful! Username: ${username}, Email: ${email}`);
});

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
